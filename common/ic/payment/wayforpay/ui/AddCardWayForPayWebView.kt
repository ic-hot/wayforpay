package ic.payment.wayforpay.ui


import ic.base.primitives.float64.ext.toString

import ic.gui.dim.dp.ByContainer
import ic.gui.scope.GuiScope
import ic.gui.scope.ext.views.web.WebView

import ic.payment.wayforpay.model.WayForPayParamsForAddCard


fun GuiScope.AddCardWayForPayWebView (

	params : WayForPayParamsForAddCard,

	onFailure : (message: String) -> Unit,

	onSuccess : () -> Unit

) = (

	WebView(
		width = ByContainer, height = ByContainer,
		pageHtml = (
			"<html>" +
				"<script>" +
					"var onLoad = function() {" +
						"document.getElementById('Form').submit();" +
					"};" +
				"</script>" +
				"<body onload='onLoad()'>" +
					"<form action='${ params.formActionUrl }' method='post' id='Form'>" +
						"<input type='hidden' name='merchantSignature' value='" + params.merchantSignature + "'>" +
						"<input type='hidden' name='apiVersion' value='" + params.apiVersion + "'>" +
						"<input type='hidden' name='merchantAccount' value='" + params.merchantAccount + "'>" +
						"<input type='hidden' name='verifyType' value='" + params.verifyType + "'>" +
						"<input type='hidden' name='paymentSystem' value='" + params.paymentSystem + "'>" +
						"<input type='hidden' name='serviceUrl' value='" + params.serviceUrl + "'>" +
						"<input type='hidden' name='amount' value='" + params.amount.toString("0.##") + "'>" +
						"<input type='hidden' name='currency' value='" + params.currency + "'>" +
						"<input type='hidden' name='merchantDomainName' value='" + params.merchantDomainName + "'>" +
						"<input type='hidden' name='orderReference' value='" + params.orderReference + "'>" +
						"<input type='hidden' name='returnUrl' value='" + params.returnUrl + "'>" +
					"</form>" +
				"</body>" +
 			"</html>"
		),
		onHtmlChanged = { url, html ->
			if (
				url == "https://secure.wayforpay.com/return" ||
				url == "https://secure.wayforpay.com/closing#" ||
				(url ?: "").contains("closing")
			) {
				when {
					html.contains("Не удалось добавить карту") -> {
						onFailure("Не удалось добавить карту")
					}
					html.contains("Не вдалося верифікувати картку") -> {
						onFailure("Не вдалося верифікувати картку")
					}
					else -> {
						onSuccess()
					}
				}
			}
		}
	)

)