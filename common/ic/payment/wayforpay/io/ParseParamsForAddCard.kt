package ic.payment.wayforpay.io


import ic.util.code.json.JsonObject
import ic.util.code.json.ext.getAsFloat64
import ic.util.code.json.ext.getAsInt32
import ic.util.code.json.ext.getAsString

import ic.payment.wayforpay.model.WayForPayParamsForAddCard


fun parseParamsForAddCard (

	paramsJson : JsonObject,

	formActionUrl : String,

	returnUrl : String

) : WayForPayParamsForAddCard {

	return WayForPayParamsForAddCard(
		formActionUrl = formActionUrl,
		returnUrl = returnUrl,
		apiVersion = paramsJson.getAsInt32("apiVersion"),
		amount = paramsJson.getAsFloat64("amount"),
		currency = paramsJson.getAsString("currency"),
		orderReference = paramsJson.getAsString("orderReference"),
		serviceUrl = paramsJson.getAsString("serviceUrl"),
		verifyType = paramsJson.getAsString("verifyType"),
		paymentSystem = paramsJson.getAsString("paymentSystem"),
		merchantAccount = paramsJson.getAsString("merchantAccount"),
		merchantDomainName = paramsJson.getAsString("merchantDomainName"),
		merchantSignature = paramsJson.getAsString("merchantSignature")
	)

}