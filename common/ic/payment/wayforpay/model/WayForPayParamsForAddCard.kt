package ic.payment.wayforpay.model


import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.Int32


data class WayForPayParamsForAddCard (

	val formActionUrl : String,
	val returnUrl : String,

	val apiVersion : Int32,

	val amount : Float64,
	val currency : String,

	val paymentSystem : String,
	val verifyType : String,

	val orderReference : String,

	val serviceUrl : String,

	val merchantAccount : String,
	val merchantDomainName : String,
	val merchantSignature : String

)